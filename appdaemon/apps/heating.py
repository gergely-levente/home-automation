import appdaemon.plugins.hass.hassapi as hass
from datetime import datetime, time
from itertools import groupby

class Heating(hass.Hass):
    HEATING_SCHEDULES = 'heating.test'

    active_timers = []

    def initialize(self):
        def log_target_temperature_change(entity_id, attribute, old, new, kwargs):
            name = new["attributes"]['friendly_name']
            temperature = new["attributes"]["temperature"]
            self.log("{} ({}) temperature is set to {}°C".format(name, entity_id, temperature))
        climates = self.get_state(entity = 'group.thermostats', attribute = 'entity_id')
        for climate in climates:
            climate_friendly_name = self.get_state(climate, attribute = 'friendly_name')
            self.listen_state(log_target_temperature_change,
                              attribute = 'all',
                              entity = climate)

        def reschedule_on_change(entity, attribute, old, new, kwargs):
            self.log("Schedule changed")
            self.reschedule(schedule = new)
        self.listen_state(reschedule_on_change,
                          entity = self.HEATING_SCHEDULES,
                          attribute ='attributes')

        schedule = self.get_state(entity = self.HEATING_SCHEDULES, attribute = 'attributes')
        # entity_exists()
        if schedule is None:
            # TODO: restore state from db manually (via HTTP?) as temporary workaround
            self.set_state('heating.test', state = 'uninitialized')
        else:
            self.reschedule(schedule = schedule)

    def reschedule(self, schedule):
        if 'active_profiles' not in schedule or 'entries' not in schedule:
            self.log('Invalid data. Schedule cannot be updated.', level='WARNING')
            return

        for timer in self.active_timers:
            self.cancel_timer(timer)
        self.active_timers = []

        active_profiles = schedule['active_profiles']
        entries = schedule['entries']
        active_entries_for_week = list((
            {
                'room_id': ap['room_id'],
                'day_id': ap['day_id'],
                'time': self.parse_time(e['time'] + ':00'),
                'temperature': e['temperature'],
            }
            for ap in active_profiles
            for e in entries
            if ap['profile_id'] == e['profile_id']
        ))

        def set_temperature(kwargs):
            self.call_service("climate/set_temperature",
                              entity_id = kwargs['room_id'],
                              temperature = kwargs['temperature'])

        days_of_week = ("mon", "tue", "wed", "thu", "fri", "sat", "sun")

        for entry in active_entries_for_week:
            timer = self.run_daily(set_temperature,
                                   entry['time'],
                                   constrain_days = days_of_week[entry['day_id']],
                                   room_id = entry['room_id'],
                                   temperature = entry['temperature'])
            self.active_timers.append(timer)

        entries_today = (e for e in active_entries_for_week
                         if e['day_id'] == self.date().weekday()
                         and e['time'] < self.time())
        entries_today_sorted = sorted(entries_today, key = lambda e: (e['room_id'], e['time']))
        entries_by_room = groupby(entries_today_sorted, key = lambda e: e['room_id'])
        for room_id, entry_group in entries_by_room:
            entry_list = list(entry_group)
            if len(entry_list):
                last_entry = entry_list[-1]
                set_temperature({
                    'room_id': last_entry['room_id'],
                    'temperature': last_entry['temperature']
                })

