import appdaemon.plugins.hass.hassapi as hass
from datetime import datetime, time
import json # csak a prettyprinthez kell

class Sprinkler(hass.Hass):
    # TODO mqtt plugin?
    SCHEDULE_SENSOR = 'sensor.sprinkler_schedules'

    active_timers = []

    def initialize(self):
        self.log('Initializing...')

        def reschedule_on_change(entity, attribute, old, new, kwargs):
            self.reschedule(schedule = new)
        self.listen_state(
            reschedule_on_change,
            entity = self.SCHEDULE_SENSOR,
            attribute = 'zones'
        )

        schedule = self.get_state(entity = self.SCHEDULE_SENSOR, attribute = 'zones')
        self.reschedule(schedule = schedule)

        def reschedule_daily(kwargs):
            self.reschedule(schedule = kwargs['schedule'])
        self.run_daily(reschedule_daily, time(0, 0, 0), schedule=schedule)

        def log_switch_state(entity, attribute, old, new, kwargs):
            self.log("{} ({}): {}".format(kwargs['friendly_name'], entity, new))
        switches = self.get_state(entity = 'group.sprinkler', attribute = 'entity_id')
        for switch in switches:
            self.log(self.get_state(switch, attribute = 'friendly_name'))
            self.listen_state(
                log_switch_state,
                entity = switch,
                friendly_name = self.get_state(switch, attribute = 'friendly_name')
            )

        self.log('Initialization complete')

    def reschedule(self, schedule):
        def set_switch_cb(kwargs):
            if kwargs['state']:
                self.turn_on(kwargs['switch'])
            else:
                self.turn_off(kwargs['switch'])

        self.log('rescheduling...')
        self.log(json.dumps(schedule, indent=2, ensure_ascii=False))

        while self.active_timers:
            self.cancel_timer(self.active_timers.pop())

        #  TODO hibakezelés: mi van ha változott a json struktúra?
        for zone in schedule:
            # self.log(zone['switch'])
            today = self.date().weekday()
            # self.log("today: " + str(today))
            for entry in zone['schedules'][today]:
                onTime = self.parse_time(entry['on'] + ':00')
                offTime = self.parse_time(entry['off'] + ':00')
                # self.log('{} - {}'.format(onTime, offTime))

                if onTime >= offTime:
                    self.log(
                        'off time must be greater than on time. skipping...',
                        level='WARNING'
                    )
                    continue;

                if self.time() >= offTime:
                    continue

                off_timer = self.run_at(
                    set_switch_cb,
                    datetime.combine(self.date(), offTime),
                    switch=zone['switch'],
                    state=False
                )
                self.active_timers.append(off_timer)

                if self.time() >= onTime and self.time() < offTime:
                    self.turn_on(zone['switch'])
                else:
                    on_timer = self.run_at(
                        set_switch_cb,
                        datetime.combine(self.date(), onTime),
                        switch=zone['switch'],
                        state=True
                    )
                    self.active_timers.append(on_timer)

        self.log('timers updated')
