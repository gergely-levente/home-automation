hass és hass-apps (heaty) python venv-be telepítve  
hass-apps telepíti appdaemon-t is  
  - hass-apps appdaemon==3.0.1 -et telepít, ez nem jó:  
  - appdaemon==3.0.0b5 kell, amíg ezt nem mergelik https://github.com/efficiosoft/hass-apps/issues/12  

mindkettőre: `chown -R homeassistant:homeassistant`

repo letöltése gitről:  
  - `cd /home/homeassistant/.homeassistant`  
  - `git init`  
  - `git remote add origin https://gergely-levente@bitbucket.org/gergely-levente/hass.git`  
  - `git fetch`  
  - `git checkout master -f`  

heaty betöltése appdaemon-ba:  
  - `cp /srv/hass-apps/lib64/python3.6/site-packages/hass_apps/data/hass_apps_loader.py /home/homeassistant/.homeassistant/appdaemon/apps/`  

 mosquitto telepítése  

frontend:  
  - `git clone https://github.com/home-assistant/home-assistant-polymer.git`  
  - `chown`  
  - EZ NEM KELL csak simán engedélyezni a frontendet, nem kell javascript_version, nem kell development_repo  

indítás:  
  - `(homeassistant)[homeassistant]$ hass`  
  - `(hass-apps)[homeassistant]$ appdaemon --config /home/homeassistant/appdaemon`  
  - `mosquitto -v -c /home/homeassistant/.homeassistant/mosquitto.conf`


  ***

  fejlesztés márik gépről:  
  ~~`sshfs  homeassistant@192.168.1.230:/home/homeassistant /mnt/homeassistant/`~~  
  `sshfs l@192.168.1.230:../homeassistant /mnt/homeassistant`
